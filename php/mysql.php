<?php
  $db_host = getenv('DB_HOST');
  $db_name = getenv('DB_NAME');
  $db_password = getenv('DB_PASSWORD');
  $db_user =  getenv('DB_USER');
  $conn = new mysqli("$db_host", "$db_user", "$db_name", "$db_password");
 
 
  if ($conn->connect_error) {
    die("ERROR: Unable to connect: " . $conn->connect_error);
  } 

  echo 'Connected to the database MySQL.<br>';


  $conn->close();
?>
